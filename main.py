#!/usr/bin/env python3

from pytrix.matrix import Matrix as mx
import sys


def create_matrix():
    rows = input("How many rows in your matrix? ")
    rows = int(rows)

    columns = input("How many columns in your matrix? ")
    columns = int(columns)

    data = []
    for row in range(0, rows):
        data.append([])
        for column in range(0, columns):
            user_input = input(f"At {row + 1} {column + 1}, enter your value: ")
            user_input = int(user_input)
            data[row].append(user_input)

    return mx(data)


print("Welcome to the Pytrix!"
      "\nPlease enter your matrix")
matrix = create_matrix()

while True:
    print("-----------------")
    print("Your matrix is: ")
    matrix.show_matrix()
    print("-----------------")
    print("What would you like to do?")
    choice = input("1. Transpose the matrix\n"
                   "2. Multiply matrix by scalar\n"
                   "3. Find determinant of the matrix\n"
                   "4. Add another matrix\n"
                   "5. Multiply by another matrix\n"
                   "6. Quit\n")
    choice = int(choice)

    print("-----------------")
    if choice == 1:
        matrix.transpose()
        print("Transposing...")
    elif choice == 2:
        value = input("By how much do you want to multiply your matrix? ")
        value = int(value)
        matrix.multiply_by_scalar(value)
    elif choice == 3:
        determinant = matrix.find_determinant()
        if determinant:
            print(f"Determinant = {determinant}")
        else:
            print("Determinant could not be found")
    elif choice == 4:
        print("You need to specify the other matrix")
        other_matrix = create_matrix()
        matrix.add(other_matrix)
    elif choice == 5:
        print("You need to specify the other matrix")
        other_matrix = create_matrix()
        matrix.multiply_by(other_matrix)
    elif choice == 6:
        sys.exit()




