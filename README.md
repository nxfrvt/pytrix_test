# Test application for pytrix module

## Table of contents
* [General info](#general-info)
* [Authors](#authors)

## General info

Simple console application for my pytrix module

## Authors

![alt text][logo]
**Bartłomiej Mazurek**
========================


[logo]: https://avatars0.githubusercontent.com/u/64803053?s=460&u=2009c05a55d5d25adb8264f8f6c419daed7e3bdb&v=4